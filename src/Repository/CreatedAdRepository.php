<?php

namespace App\Repository;

use App\Entity\CreatedAd;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CreatedAd|null find($id, $lockMode = null, $lockVersion = null)
 * @method CreatedAd|null findOneBy(array $criteria, array $orderBy = null)
 * @method CreatedAd[]    findAll()
 * @method CreatedAd[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CreatedAdRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CreatedAd::class);
    }

//    /**
//     * @return CreatedAd[] Returns an array of CreatedAd objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CreatedAd
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
